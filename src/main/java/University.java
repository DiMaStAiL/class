import Interface_.GetInf;
public class University implements GetInf {
    private String Name;        //Имя
    private String Address;      //Адрес
    private int Code;           //Код
    private String[] list_specialty = {"1","2","3","4"}; //Список специальностей
    //Конструктор по умолчанию
    public University(){}

    //Констуктор с инициализацией полей
    public University(String name, int code, String address){
        this.Code = code;
        this.Name=name;
        this.Address = address;
    }

    //Личные методы класса
    public void getListOfSpecialty(){
        for (String i: list_specialty) {
            System.out.println(i);
        }
    }

    //Метод интерфейса
    @Override
    public void getInfObject() {
        System.out.println("Информация об университете: имя - " + Name + " код - " + Code + " адрес - " + Address);
    }
}
