package Human;
import Interface_.GetInf;

public class Student extends Person implements GetInf {
    //Поля
    protected double Agsum; //средний бал

    //Конструкторы
    public Student(){}
    public Student(String name, int code, double agsum, int age, String address){
        this.Firstname = name;
        this.Code = code;
        this.Agsum = agsum;
        this.Age = age;
        this.Address = address;
    }

    //Методы
    //Проверка на получение стипендии
    public boolean scholarship(){return this.Agsum >= 4.5; }
    //Метод родителя
    @Override
    public void Hello() { System.out.println("Привет! Я студент " + Firstname); }

    //Гет сет
    public double getAgsum() { return this.Agsum; }
    public void setAgsum(double agsum) { this.Agsum = agsum;   }

    //Метод интерфейса
    @Override
    public void getInfObject() {
        System.out.println("Информация об студенте: имя - " + Firstname + " код - " + Code + " адрес - " + Address + " , возраст - " + Age + " , средний бал - " + Agsum);
    }
}
