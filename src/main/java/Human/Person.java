package Human;
abstract public class Person   {
    //Поля
    protected int Age;
    protected int Code;             //Личный код
    protected String Firstname;     //Имя человека
    protected String Address;       //Адрес проживания

    //геттеры - сеттеры
    public void setCode(int code) {
        this.Code = code;
    }
    public int getCode() {return this.Code;}

    public void setFirstname(String name){
        this.Firstname = name;
    }
    public String getFirstName(){return Firstname;}

    public void setAddress(String address){this.Address=address;}
    public String getAddress() { return Address; }

    public int getAge() { return Age; }
    public void setAge(int age) { this.Age = age; }

    //Методы
    public void Birthday(){
        System.out.println(this.Firstname + " празнует день рождения! " + this.Firstname + " исполнилось " + (++Age));
    }

    //Адстрактный метод
    public abstract void Hello();
}
