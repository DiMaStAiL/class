package Human;
import Interface_.GetInf;

public class Teacher extends Person implements GetInf {
    protected int Rang;             //Розряд, ученая степень
    protected String Discipline;    //Дисциплина преподавания

    //Конструкторо по умолчанию
    public Teacher(){}

    //Констуктор с инициализацией полей
    public Teacher(String name, int code, int rang, String address, String discipline){
        this.Firstname = name;
        this.Code = code;
        this.Address = address;
        this.Rang = rang;
        this.Discipline = discipline;
    }

    //Геттеры - сеттеры
    public int getRang() {return this.Rang; }
    public void setRang(int rang) {this.Rang = rang; }

    //Методы
    //Изминение бала студента
    public void changeStudAgSum(Student name, double bal){name.setAgsum(bal);}

    //Метод родителя
    @Override
    public void Hello() { System.out.println("Здраствуйте! Я преподаватель " + Firstname); }

    //Метод интерфейса
    @Override
    public void getInfObject() {
        System.out.println("Информация об преподавателе: имя - " + Firstname + " код - " + Code + " адрес - " + Address + " , возраст - " + Age + " , ранг - " + Rang);

    }
}
