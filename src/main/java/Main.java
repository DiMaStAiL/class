import Human.Person;
import Human.Student;
import Human.Teacher;
import Interface_.GetInf;


public class Main {
    public static void main(String[] args) {

        Student Stud_1 = new Student("Дима", 19200, 5, 100, "НЕважно");
        Student Stud_2 = new Student("НЕДима", 17345, 4, 34, "Важно");

        Teacher Teach_1 = new Teacher("Олександр", 18293, 19, "Кому нужен адрес?", "Математика");
        Teacher Teach_2 = new Teacher("Виктор", 12513, 17, "Да что такое", "Физика");

        //При изминении на родительскую ссылку, методы вызвать нельзя
        //При переопределении родительского метода в дочернем класе, вызываеться метод дочернего
        //Статическаие методы нельзя переопределять. Нужно тогда всем дочерним методам прописывать static, но тогда это будут другие методы

        Student Stud_3 = new Student();
        Stud_3.setFirstname("Nicklaus");
        Stud_3.setCode(13786);
        Stud_3.setAgsum(4.4);
        Stud_3.setAddress("Ужас");
        Stud_3.setAge(1025);

        Stud_1.Hello();
        Stud_2.Hello();
        Stud_3.Hello();

        Teach_1.Hello();
        Teach_2.Hello();

        System.out.println("Средний бал " + Stud_2.getFirstName()+ " : " + Stud_2.getAgsum());
        System.out.println("Стипендия: " + Stud_2.getFirstName()+ " " + Stud_2.scholarship());
        Teach_1.changeStudAgSum(Stud_2, 5);
        System.out.println("Средний бал " + Stud_2.getFirstName()+ " : " + Stud_2.getAgsum());
        System.out.println("Стипендия: " + Stud_2.getFirstName()+ " " + Stud_2.scholarship());

        System.out.println( Stud_3.getAge());
        Stud_3.Birthday();
        System.out.println( Stud_3.getAge());

        Person[] personArr ={Stud_1,Stud_2,Stud_3,Teach_1,Teach_2};
        for (Person a:personArr) {
            a.Hello();
        }
        //Если добавить все обьекты в масив типа родительского класа, то через цикл можна вызвать методы родительского класса

        //Новый клас университет
        University University_1 = new University("HimTex", 14321, "Infinity?");
        University_1.getListOfSpecialty();

        //Реализация интерфейса
        GetInf[] arrayObj = {(GetInf) Stud_1, (GetInf) Stud_2, (GetInf) Stud_3, (GetInf) Teach_1, (GetInf) Teach_2, (GetInf) University_1};
        for (GetInf a: arrayObj) {
            a.getInfObject();
        }
        //Тут доступны методы описаны в интерфейсе
    }

}
